/* *****************************************************************************
 FILE_NAME:     APP_INCLUDES.h
 DESCRIPTION:   Application header file for AXIOM-01 project
 DESIGNER:      GESSE JUSTINIANO / PLINIO BARBOSA
 CREATION_DATE: JAN/2019
 VERSION:       1.0
***************************************************************************** */

/* Library access definition                                                  */
#ifndef SOURCES_APP_INCLUDES_H_
#define SOURCES_APP_INCLUDES_H_

/* Application specific includes                                              */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>


#include "APP.h"
#include "VARMAP.h"
#include "DATALOGGER_Link.h"
#include "DATALOGGER_Link.h"

#include "LogCoordinator.h"
#include "VARMAP.h"
#include "STORAGE_Link.h"


/* Support routine includes                                                   */
#ifdef ENABLE_SEGGER_RTT_DEBUG
#include "SEGGER_RTT.h"
#endif

/* Support routine includes                                                   */
#ifdef ENABLE_SEGGER_RTT_DEBUG
#include "SEGGER_RTT.h"
#endif

#endif /* SOURCES_APP_APP_INCLUDES_H_ */


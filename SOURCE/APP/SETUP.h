#ifndef SETUP_FREEDOM_VARS_H_INCLUDE
#define SETUP_FREEDOM_VARS_H_INCLUDE

/******************************************************************************/
/*        DEFINES, ENUMS, STRUCTURES                                          */
/******************************************************************************/
/* Definition to a value to be used                                           */
/* as an irrelevant filling                                                   */
#define DONTCARE                                                      0xFFFFFFFF
#define NULL_VALUE                                                             0
#define NaN                                                   (float)(0.0F/0.0F)

/******************************************************************************/
/*        BOARD SELECTION                                                     */
/******************************************************************************/
#define PLATFORM_RELEASE                                                       1
#define PLATFORM_DEVELOPMENT                                                   2
/* Select below which is the platform being run:                              */
/* RELEASE - Proper release board (AVM-01)                                    */
/* DEVELOPMENT - Usage with debug platform Board                              */
#define CURRENT_PLATFORM                                      PLATFORM_RELEASE
/******************************************************************************/
/*        DEVICE RTC M41T82 CONFIGURATION                                     */
/******************************************************************************/
/* Select below IIC  ID that will be available for the device.                */
#define DEV_M41T82_IIC_ID                                                      1

/******************************************************************************/
/*        SPI DRIVER SPECIFIC CONFIGURATION                                   */
/******************************************************************************/
/* Define below the buffer size for the SPI transactions. Main points:        */
/* - Two buffers with this size will be allocated for each SPI peripheral     */
/* - Maximum send size is equal to this length.                               */
/* - Maximum receive size is equal to this length minus the command length.   */
#define SPI_BUF_LENGTH                                                      2064
/* Define below how many IDs the driver will support.                         */
#define SPI_MAX_ID_LIMIT                                                       4

/******************************************************************************/
/*        SPI DRIVER IDs                                                      */
/******************************************************************************/
#define ETHC_SPI_ID                                                           1
#define FLASH_MICRON_SPI_ID                                                   2
/******************************************************************************/
/*        PIT DRIVER IDs                                                      */
/******************************************************************************/
#define MMC_FLASH_MICRON_PIT_ID                                   (uint8)      7
#define MMC_DEV_M41T82_PIT_ID_LIST                                         31,32
/***** EEPROM IDs *************************************************************/
#define MMC_01_EEPROM_ID_NVV_0                                  (uint8)        0
#define MMC_01_EEPROM_ID_NVV_1                                  (uint8)        1
#define MMC_01_EEPROM_ID_MDB_0                                  (uint8)        2
#define MMC_01_EEPROM_ID_MDB_1                                  (uint8)        3

/* Set below the SPI parameters                                               */
#define FLASH_MICRON_SPI_PORT                  SPI2_AT_PORT_B_SCK21_SOUT22_SIN23
#define FLASH_MICRON_SPI_CHIP_SELECT                              CS_PORTA_PIN14
#define FLASH_MICRON_SPI_BAUDRATE                                      SPI_1Mbps
#define FLASH_MICRON_PIT_ID                              MMC_FLASH_MICRON_PIT_ID
/******************************************************************************/
/*        DEVICE RTC M41T82 CONFIGURATION                                     */
/******************************************************************************/
/* Set below the IIC Configuration to interface with the device.              */
#define DEV_M41T82_IIC_INTERFACE                                  IIC0_INTERFACE
#define DEV_M41T82_IIC_SDA                                          SDA0_AT_PTB1
#define DEV_M41T82_IIC_SCL                                          SCL0_AT_PTB0
#define DEV_M41T82_IIC_BAUDRATE                                       IIC_50kbps
#define DEV_M41T82_PIT_ID_LIST                        MMC_DEV_M41T82_PIT_ID_LIST
/* Select below the maximum number of alarms that the device will handle.     */
#define DEV_M41T82_NUMBER_OF_ALARMS                                            5
/******************************************************************************/
/*        DATALOGGER SPECIFIC CONFIGURATION                                   */
/******************************************************************************/
/* Maximum available ID's.                                                    */
#define MAX_DLOG_ID_AVAILABLE                                                  5

/* Buffer length for write and read internal operations.                      */
#define MAX_DLOG_BUF_LENGTH                          255//( SPI_BUF_LENGTH - 8 )

#define AXIOM_01_TD_LOG_EEP_ID                                  (uint8)        0
#define AXIOM_01_BD_LOG_EEP_ID                                  (uint8)        1
#define AXIOM_01_SD_LOG_EEP_ID                                  (uint8)        2
#define AXIOM_01_AD_LOG_EEP_ID                                  (uint8)        3
#define AXIOM_01_EVD_LOG_EEP_ID                                 (uint8)        4
/******************************************************************************/
/*        STORAGE SPECIFIC CONFIGURATION                                      */
/******************************************************************************/
#define STORAGE_ID_NVV_0                                  MMC_01_EEPROM_ID_NVV_0
#define STORAGE_ID_NVV_1                                  MMC_01_EEPROM_ID_NVV_1
#define STORAGE_ID_MDB_0                                  MMC_01_EEPROM_ID_MDB_0
#define STORAGE_ID_MDB_1                                  MMC_01_EEPROM_ID_MDB_1
/******************************************************************************/
/*        VUR SPECIFIC CONFIGURATION                                          */
/******************************************************************************/
/* Limit the VUR's queue size, as RAM is short and it is not expected to have */
/*  much activity at the same time.                                           */
#define VUR_SIZE                                                              32
/******************************************************************************/
/*        DEFINES, ENUMS, STRUCTURES                                          */
/******************************************************************************/
/* SPI Channels / CS Pins                                                     */
#define SPI_USING_SPI0                                                     FALSE
#define SPI_USING_SPI1                                                      TRUE
#define SPI_USING_SPI2                                                      TRUE
/******************************************************************************/
/*        PIT DRIVER SPECIFIC CONFIGURATION                                   */
/******************************************************************************/
/* Select below the main PIT index. THE DRIVER WILL USE THE SELECTED ONE PLUS */
/*  THE ONE BELOW IT. They will be used in chained mode.                      */
#define PIT_PERIPHERAL                                                         3  /* PIT3 and PIT2 will be used */
/* Select below the maximum number of IDs that the driver will accept.        */
#define PIT_MAX_TIMERS_AVAILABLE                                              50
/******************************************************************************/
/*        FLASH MICRON DEVICE SPECIFIC CONFIGURATION                          */
/******************************************************************************/
/* Set below the main parameters                                              */
#define FLASH_MICRON_ID_LIMIT                                                  5
#define FLASH_MICRON_SPARE_MEM_BYTES                                         128
#endif

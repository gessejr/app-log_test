/*
 * Debug.c
 *
 *  Created on: 17/04/2015
 *      Author: denisberaldo
 */

#include "Debug.h"

void Init_Debug(void)
{
  /* Debug Pin, Port and ID selection are now performed on SETUP.h file */
  #if DEBUG_MODE == DEBUG_ENABLED
    static GPIO_Parameters GPIO_Param;

    GPIO_Param.Pin = LED_RED_GPIO_PIN;
    GPIO_Param.Port = LED_RED_GPIO_PORT;
    GPIO_Param.DataDirection = GPIO_Output;
    GPIO_Param.DataOutput = 0;
    (void)GPIO_Config(LED_RED_GPIO_ID, &GPIO_Param);

    GPIO_Param.Pin = LED_GREEN_GPIO_PIN;
    GPIO_Param.Port = LED_GREEN_GPIO_PORT;
    GPIO_Param.DataDirection = GPIO_Output;
    GPIO_Param.DataOutput = 0;
    (void)GPIO_Config(LED_GREEN_GPIO_ID, &GPIO_Param);

    GPIO_Param.Pin = LED_YELLOW_GPIO_PIN;
    GPIO_Param.Port = LED_YELLOW_GPIO_PORT;
    GPIO_Param.DataDirection = GPIO_Output;
    GPIO_Param.DataOutput = 0;
    (void)GPIO_Config(LED_YELLOW_GPIO_ID, &GPIO_Param);

    (void)Set_Timer(DEBUG_PIT_ID, 100, MiliSec);
  #endif
}

void Run_Debug(void)
{
  #define FREQUENCY_SOUND           50 /* Hz */
  #define PIT_FREQ                  (1000 / FREQUENCY_SOUND)
  #define LED_TOGGLE                (100 / PIT_FREQ)
  
  #if DEBUG_MODE == DEBUG_ENABLED

  static uint8  LedRedCount = 0;
#if CURRENT_PLATFORM == PLATFORM_RELEASE
  static uint16 TimeCount = 0;
  static bool   isSoundPlaying = FALSE;
#endif

    if (Get_Timer(DEBUG_PIT_ID) != OPERATION_RUNNING)
    {
      (void)Set_Timer(DEBUG_PIT_ID, PIT_FREQ, MiliSec);
      
      if(LedRedCount++ > LED_TOGGLE )
      {
        LED_RED_TOGGLE();
        LedRedCount = 0;
      }

      #if CURRENT_PLATFORM == PLATFORM_RELEASE
        if( isSoundPlaying != FALSE )
        {
          if(TimeCount == 0)
          {
            TimeCount = ( 2000 / PIT_FREQ );
            isSoundPlaying = FALSE;
          }

          LED_GREEN_TOGGLE();
        }
        else
        {
          if(TimeCount == 0)
          {
            TimeCount = ( 200 / PIT_FREQ );
            isSoundPlaying = TRUE;
          }

          LED_GREEN_OFF();
        }
        --TimeCount;
      #endif
    }
  #endif
}

/* Compiler check */
#ifndef DEBUG_MODE
  #warning "DEBUG: Warning! DEBUG MODE define not found! Is it present on SETUP.h?"
#endif

#if DEBUG_MODE == DEBUG_ENABLED
  #warning "DEBUG: Warning! Debug pins and PIT are enabled and allocated!"
#endif

/* *****************************************************************************
 *        INCLUDES (and DEFINES for INCLUDES)
 ***************************************************************************** */
#include "APP_INCLUDES.h"

#define DATALOGGER_ONLY                                                        0
#define STORAGE_ONLY                                                           1
#define ALL_INITIALIZED                                                        2

#define APP_TYPE                                                 ALL_INITIALIZED



/* *****************************************************************************
 *
 *                FUNCTIONS
 *
 ******************************************************************************/
/* *****************************************************************************
 *
 *                DEFINES, ENUMS, STRUCT
 *
 ******************************************************************************/


CREATE_SINGLE_DIAGNOS_VAL(  uint16              , TransactionID                                            , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint16              , UserID                                                   , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint32              , TransStartDate                                           , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint32              , TransStartTime                                           , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint32              , TransStopDate                                            , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint32              , TransStopTime                                            , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint8               , WMIntended                                               , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  float               , BaseTemperature                                          , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint16              , PresetType                                               , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint16              , StartAddrBatch                                           , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint16              , EndAddrBatch                                             , 0x0000                  );

CREATE_SINGLE_DIAGNOS_VAL(  uint16              , BatchID                                                  , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint8               , ArmNumber                                                , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint16              , ProductName                                              , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint32              , BatchStartDate                                           , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint32              , BatchStartTime                                           , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint32              , BatchStopDate                                            , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint32              , BatchStopTime                                            , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint8               , WMCompilant                                              , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  float               , PresetVolume                                             , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  float               , ReturnedQuantity                                         , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  float               , BatchGOV                                                 , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  float               , BatchGSV                                                 , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  float               , BatchMass                                                , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint16              , BlendType                                                , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint16              , StartAddrStream                                          , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint16              , EndAddrStream                                            , 0x0000                  );

CREATE_SINGLE_DIAGNOS_VAL(  uint16              , StreamID                                                 , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint16              , AccumStartGOV                                            , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint16              , AccumStopGOV                                             , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint16              , AccumStartGSV                                            , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint16              , AccumStopGSV                                             , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  float               , AccumStartMass                                           , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  float               , AccumStopMass                                            , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  float               , StreamGOV                                                , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  float               , StreamGSV                                                , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  float               , StreamMass                                               , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  float               , AverageDensity                                           , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  float               , TempCompensation                                         , 0x0000                  );

CREATE_SINGLE_DIAGNOS_VAL(  uint16              , AlarmLogRegion1                                          , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint16              , AlarmLogRegion2                                          , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint16              , AlarmLogRegion3                                          , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint16              , AlarmLogRegion4                                          , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint16              , AlarmLogRegion5                                          , 0x0000                  );

CREATE_SINGLE_DIAGNOS_VAL(  uint16              , EventType                                                , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint32              , EventDate                                                , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint32              , EventTime                                                , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint32              , GenericArea1                                             , 0x0000                  );
CREATE_SINGLE_DIAGNOS_VAL(  uint32              , GenericArea2                                             , 0x0000                  );

CREATE_SINGLE_DIAGNOS_VAL(  bool                , LogTriggerTD        , FALSE                                                        );
CREATE_SINGLE_DIAGNOS_VAL(  bool                , LogTriggerSD        , FALSE                                                        );
CREATE_SINGLE_DIAGNOS_VAL(  bool                , LogTriggerBD        , FALSE                                                        );
CREATE_SINGLE_DIAGNOS_VAL(  bool                , LogTriggerAD        , FALSE                                                        );
CREATE_SINGLE_DIAGNOS_VAL(  bool                , LogTriggerEVD       , FALSE                                                        );

enum{
  START = 0, END,
}Test_Status;

bool System_Initialized = FALSE;
int main(void)
{
  STORAGE_Set.RestoreMDB = TRUE;
  STORAGE_Set.RestoreNVV = TRUE;
  while(1)
  {
    #if APP_TYPE == DATALOGGER_ONLY
      DATALOGGER();
    #endif

    #if APP_TYPE == STORAGE_ONLY
      STORAGE();
    #endif

    #if APP_TYPE == ALL_INITIALIZED
      DATALOGGER();
      STORAGE();
    #endif

    switch(Test_Status)
    {
      case START:
        /* Dummy Data */
        EventType.Value     = 2816;
        EventDate.Value     = 10  ;
        EventTime.Value     = 10  ;
        GenericArea1.Value  = 0   ;
        GenericArea2.Value  = 0   ;

        LogTriggerSD.Value  = TRUE;
        LogTriggerTD.Value  = TRUE;
        LogTriggerBD.Value  = TRUE;
        LogTriggerEVD.Value = TRUE;

        Test_Status = END;
        break;

      case END:
        if( DATALOGGER_Get.WriteStatus[Transaction_Log] == DLOG_ST_FINISHED && DATALOGGER_Get.WriteStatus[Stream_Log] == DLOG_ST_FINISHED &&  DATALOGGER_Get.WriteStatus[Batch_Log] == DLOG_ST_FINISHED   && DATALOGGER_Get.WriteStatus[Event_Log] == DLOG_ST_FINISHED  )
        {
          Test_Status = START;
        }
        break;
    }
  }
}

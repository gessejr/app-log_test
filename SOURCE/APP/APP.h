#ifndef SOURCE_APP_APP_H_
#define SOURCE_APP_APP_H_

#include "types.h"
#include "macros.h"
#include "SETUP.h"

typedef enum
{
  LIST_DATALOG       = 0,
  LIST_READ_INPUTS      ,
  LIST_READ_TEMP        ,
  LIST_VARIABLES        ,
  LIST_READ_MAXMIN      ,
  LIST_READ_8BIT        ,
} MdbMasterTags;

typedef enum
{
  Transaction_Log,
  Batch_Log,
  Stream_Log,
  Alarm_Log,
  Event_Log,
} DLog_Logs;


EXTERN_SINGLE_DIAGNOS_VAL(  uint16              , TransactionID                                            );
EXTERN_SINGLE_DIAGNOS_VAL(  uint16              , UserID                                                   );
EXTERN_SINGLE_DIAGNOS_VAL(  uint32              , TransStartDate                                           );
EXTERN_SINGLE_DIAGNOS_VAL(  uint32              , TransStartTime                                           );
EXTERN_SINGLE_DIAGNOS_VAL(  uint32              , TransStopDate                                            );
EXTERN_SINGLE_DIAGNOS_VAL(  uint32              , TransStopTime                                            );
EXTERN_SINGLE_DIAGNOS_VAL(  uint8               , WMIntended                                               );
EXTERN_SINGLE_DIAGNOS_VAL(  float               , BaseTemperature                                          );
EXTERN_SINGLE_DIAGNOS_VAL(  uint16              , PresetType                                               );
EXTERN_SINGLE_DIAGNOS_VAL(  uint16              , StartAddrBatch                                           );
EXTERN_SINGLE_DIAGNOS_VAL(  uint16              , EndAddrBatch                                             );

EXTERN_SINGLE_DIAGNOS_VAL(  uint16              , BatchID                                                  );
EXTERN_SINGLE_DIAGNOS_VAL(  uint8               , ArmNumber                                                );
EXTERN_SINGLE_DIAGNOS_VAL(  uint16              , ProductName                                              );
EXTERN_SINGLE_DIAGNOS_VAL(  uint32              , BatchStartDate                                           );
EXTERN_SINGLE_DIAGNOS_VAL(  uint32              , BatchStartTime                                           );
EXTERN_SINGLE_DIAGNOS_VAL(  uint32              , BatchStopDate                                            );
EXTERN_SINGLE_DIAGNOS_VAL(  uint32              , BatchStopTime                                            );
EXTERN_SINGLE_DIAGNOS_VAL(  uint8               , WMCompilant                                              );
EXTERN_SINGLE_DIAGNOS_VAL(  float               , PresetVolume                                             );
EXTERN_SINGLE_DIAGNOS_VAL(  float               , ReturnedQuantity                                         );
EXTERN_SINGLE_DIAGNOS_VAL(  float               , BatchGOV                                                 );
EXTERN_SINGLE_DIAGNOS_VAL(  float               , BatchGSV                                                 );
EXTERN_SINGLE_DIAGNOS_VAL(  float               , BatchMass                                                );
EXTERN_SINGLE_DIAGNOS_VAL(  uint16              , BlendType                                                );
EXTERN_SINGLE_DIAGNOS_VAL(  uint16              , StartAddrStream                                          );
EXTERN_SINGLE_DIAGNOS_VAL(  uint16              , EndAddrStream                                            );

EXTERN_SINGLE_DIAGNOS_VAL(  uint16              , StreamID                                                 );
EXTERN_SINGLE_DIAGNOS_VAL(  uint16              , AccumStartGOV                                            );
EXTERN_SINGLE_DIAGNOS_VAL(  uint16              , AccumStopGOV                                             );
EXTERN_SINGLE_DIAGNOS_VAL(  uint16              , AccumStartGSV                                            );
EXTERN_SINGLE_DIAGNOS_VAL(  uint16              , AccumStopGSV                                             );
EXTERN_SINGLE_DIAGNOS_VAL(  float               , AccumStartMass                                           );
EXTERN_SINGLE_DIAGNOS_VAL(  float               , AccumStopMass                                            );
EXTERN_SINGLE_DIAGNOS_VAL(  float               , StreamGOV                                                );
EXTERN_SINGLE_DIAGNOS_VAL(  float               , StreamGSV                                                );
EXTERN_SINGLE_DIAGNOS_VAL(  float               , StreamMass                                               );
EXTERN_SINGLE_DIAGNOS_VAL(  float               , AverageDensity                                           );
EXTERN_SINGLE_DIAGNOS_VAL(  float               , TempCompensation                                         );

EXTERN_SINGLE_DIAGNOS_VAL(  uint16              , AlarmLogRegion1                                          );
EXTERN_SINGLE_DIAGNOS_VAL(  uint16              , AlarmLogRegion2                                          );
EXTERN_SINGLE_DIAGNOS_VAL(  uint16              , AlarmLogRegion3                                          );
EXTERN_SINGLE_DIAGNOS_VAL(  uint16              , AlarmLogRegion4                                          );
EXTERN_SINGLE_DIAGNOS_VAL(  uint16              , AlarmLogRegion5                                          );

EXTERN_SINGLE_DIAGNOS_VAL(  uint16              , EventType                                                );
EXTERN_SINGLE_DIAGNOS_VAL(  uint32              , EventDate                                                );
EXTERN_SINGLE_DIAGNOS_VAL(  uint32              , EventTime                                                );
EXTERN_SINGLE_DIAGNOS_VAL(  uint32              , GenericArea1                                             );
EXTERN_SINGLE_DIAGNOS_VAL(  uint32              , GenericArea2                                             );

EXTERN_SINGLE_DIAGNOS_VAL(  bool                , LogTriggerTD        );
EXTERN_SINGLE_DIAGNOS_VAL(  bool                , LogTriggerSD        );
EXTERN_SINGLE_DIAGNOS_VAL(  bool                , LogTriggerBD        );
EXTERN_SINGLE_DIAGNOS_VAL(  bool                , LogTriggerAD        );
EXTERN_SINGLE_DIAGNOS_VAL(  bool                , LogTriggerEVD       );


#endif /* SOURCE_APP_APP_H_ */

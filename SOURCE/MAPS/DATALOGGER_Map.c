/* *****************************************************************************
 FILE_NAME:     DATALOGGER_Map.c
 DESCRIPTION:   Datalogger map
 DESIGNER:      Andre F. N. Dainese
 CREATION_DATE: 08/11/2017
 VERSION:       1.0
***************************************************************************** */

/* *****************************************************************************
 *        INCLUDES (AND DEFINES FOR INCLUDES)
***************************************************************************** */
#include "DATALOGGER_Map.h"
#include "DATALOGGER_Ctrl.h"
#include "DATALOGGER_Link.h"
#include "APP.h"
#include "LogCoordinator.h"
#define ENABLE_FLASH_FUNCTIONS
#define ENABLE_FLASH_LOWER_OP_FUNCTIONS
#define ENABLE_FLASH_LOWER_MNG_FUNCTIONS
#include "FLASH.H"
/* *****************************************************************************
 *        TRANSACTION DATALOGGER INTERNAL DECLARATION
***************************************************************************** */
              /*----------------------------------------------------------------*/
              /*  Variable Name                               , Prefix , Local  */
              /*----------------------------------------------------------------*/
COPY_SINGLE_VAR(  TransactionID                               , TD     , INTERN );
COPY_SINGLE_VAR(  UserID                                      , TD     , INTERN );
COPY_SINGLE_VAR(  TransStartDate                              , TD     , INTERN );
COPY_SINGLE_VAR(  TransStartTime                              , TD     , INTERN );
COPY_SINGLE_VAR(  TransStopDate                               , TD     , INTERN );
COPY_SINGLE_VAR(  TransStopTime                               , TD     , INTERN );
COPY_SINGLE_VAR(  WMIntended                                  , TD     , INTERN );
COPY_SINGLE_VAR(  BaseTemperature                             , TD     , INTERN );
COPY_SINGLE_VAR(  PresetType                                  , TD     , INTERN );
COPY_SINGLE_VAR(  StartAddrBatch                              , TD     , INTERN );
COPY_SINGLE_VAR(  EndAddrBatch                                , TD     , INTERN );


CREATE_DLOG_MAP(TD, INTERN)
{
               /*---------------------------------------------------------------*/
               /*  Variable Name                                       , Prefix */
               /*---------------------------------------------------------------*/
  ADD_SINGLE_VAR(  TransactionID                                       , TD     ),
  ADD_SINGLE_VAR(  UserID                                              , TD     ),
  ADD_SINGLE_VAR(  TransStartDate                                      , TD     ),
  ADD_SINGLE_VAR(  TransStartTime                                      , TD     ),
  ADD_SINGLE_VAR(  TransStopDate                                       , TD     ),
  ADD_SINGLE_VAR(  TransStopTime                                       , TD     ),
  ADD_SINGLE_VAR(  WMIntended                                          , TD     ),
  ADD_SINGLE_VAR(  BaseTemperature                                     , TD     ),
  ADD_SINGLE_VAR(  PresetType                                          , TD     ),
  ADD_SINGLE_VAR(  StartAddrBatch                                      , TD     ),
  ADD_SINGLE_VAR(  EndAddrBatch                                        , TD     ),

};


/* *****************************************************************************
 *        BATCH DATALOGGER INTERNAL DECLARATION
***************************************************************************** */
              /*----------------------------------------------------------------*/
              /*  Variable Name                               , Prefix , Local  */
              /*----------------------------------------------------------------*/
COPY_SINGLE_VAR(  BatchID                                     , BD     , INTERN );
COPY_SINGLE_VAR(  ArmNumber                                   , BD     , INTERN );
COPY_SINGLE_VAR(  ProductName                                 , BD     , INTERN );
COPY_SINGLE_VAR(  BatchStartDate                              , BD     , INTERN );
COPY_SINGLE_VAR(  BatchStartTime                              , BD     , INTERN );
COPY_SINGLE_VAR(  BatchStopDate                               , BD     , INTERN );
COPY_SINGLE_VAR(  BatchStopTime                               , BD     , INTERN );
COPY_SINGLE_VAR(  WMCompilant                                 , BD     , INTERN );
COPY_SINGLE_VAR(  PresetVolume                                , BD     , INTERN );
COPY_SINGLE_VAR(  ReturnedQuantity                            , BD     , INTERN );
COPY_SINGLE_VAR(  BatchGOV                                    , BD     , INTERN );
COPY_SINGLE_VAR(  BatchGSV                                    , BD     , INTERN );
COPY_SINGLE_VAR(  BatchMass                                   , BD     , INTERN );
COPY_SINGLE_VAR(  BlendType                                   , BD     , INTERN );
COPY_SINGLE_VAR(  StartAddrStream                             , BD     , INTERN );
COPY_SINGLE_VAR(  EndAddrStream                               , BD     , INTERN );


CREATE_DLOG_MAP(BD, INTERN)
{
               /*---------------------------------------------------------------*/
               /*  Variable Name                                       , Prefix */
               /*---------------------------------------------------------------*/
  ADD_SINGLE_VAR(   BatchID                                            , BD     ),
  ADD_SINGLE_VAR(   ArmNumber                                          , BD     ),
  ADD_SINGLE_VAR(   ProductName                                        , BD     ),
  ADD_SINGLE_VAR(   BatchStartDate                                     , BD     ),
  ADD_SINGLE_VAR(   BatchStartTime                                     , BD     ),
  ADD_SINGLE_VAR(   BatchStopDate                                      , BD     ),
  ADD_SINGLE_VAR(   BatchStopTime                                      , BD     ),
  ADD_SINGLE_VAR(   WMCompilant                                        , BD     ),
  ADD_SINGLE_VAR(   PresetVolume                                       , BD     ),
  ADD_SINGLE_VAR(   ReturnedQuantity                                   , BD     ),
  ADD_SINGLE_VAR(   BatchGOV                                           , BD     ),
  ADD_SINGLE_VAR(   BatchGSV                                           , BD     ),
  ADD_SINGLE_VAR(   BatchMass                                          , BD     ),
  ADD_SINGLE_VAR(   BlendType                                          , BD     ),
  ADD_SINGLE_VAR(   StartAddrStream                                    , BD     ),
  ADD_SINGLE_VAR(   EndAddrStream                                      , BD     ),
};


/* *****************************************************************************
 *        STREAM DATALOGGER INTERNAL DECLARATION
***************************************************************************** */
              /*----------------------------------------------------------------*/
              /*  Variable Name                               , Prefix , Local  */
              /*----------------------------------------------------------------*/
COPY_SINGLE_VAR(  StreamID                                    , SD     , INTERN );
COPY_SINGLE_VAR(  ProductName                                 , SD     , INTERN );
COPY_SINGLE_VAR(  AccumStartGOV                               , SD     , INTERN );
COPY_SINGLE_VAR(  AccumStopGOV                                , SD     , INTERN );
COPY_SINGLE_VAR(  AccumStartGSV                               , SD     , INTERN );
COPY_SINGLE_VAR(  AccumStopGSV                                , SD     , INTERN );
COPY_SINGLE_VAR(  AccumStartMass                              , SD     , INTERN );
COPY_SINGLE_VAR(  AccumStopMass                               , SD     , INTERN );
COPY_SINGLE_VAR(  StreamGOV                                   , SD     , INTERN );
COPY_SINGLE_VAR(  StreamGSV                                   , SD     , INTERN );
COPY_SINGLE_VAR(  StreamMass                                  , SD     , INTERN );
COPY_SINGLE_VAR(  AverageDensity                              , SD     , INTERN );
COPY_SINGLE_VAR(  TempCompensation                            , SD     , INTERN );


CREATE_DLOG_MAP(SD, INTERN)
{
               /*---------------------------------------------------------------*/
               /*  Variable Name                                       , Prefix */
               /*---------------------------------------------------------------*/
  ADD_SINGLE_VAR(   StreamID                                           , SD     ),
  ADD_SINGLE_VAR(   ProductName                                        , SD     ),
  ADD_SINGLE_VAR(   AccumStartGOV                                      , SD     ),
  ADD_SINGLE_VAR(   AccumStopGOV                                       , SD     ),
  ADD_SINGLE_VAR(   AccumStartGSV                                      , SD     ),
  ADD_SINGLE_VAR(   AccumStopGSV                                       , SD     ),
  ADD_SINGLE_VAR(   AccumStartMass                                     , SD     ),
  ADD_SINGLE_VAR(   AccumStopMass                                      , SD     ),
  ADD_SINGLE_VAR(   StreamGOV                                          , SD     ),
  ADD_SINGLE_VAR(   StreamGSV                                          , SD     ),
  ADD_SINGLE_VAR(   StreamMass                                         , SD     ),
  ADD_SINGLE_VAR(   AverageDensity                                     , SD     ),
  ADD_SINGLE_VAR(   TempCompensation                                   , SD     ),
};


/* *****************************************************************************
 *        ALARM DATALOGGER INTERNAL DECLARATION
***************************************************************************** */
              /*----------------------------------------------------------------*/
              /*  Variable Name                               , Prefix , Local  */
              /*----------------------------------------------------------------*/
COPY_SINGLE_VAR(  AlarmLogRegion1                             , AD     , INTERN );
COPY_SINGLE_VAR(  AlarmLogRegion2                             , AD     , INTERN );
COPY_SINGLE_VAR(  AlarmLogRegion3                             , AD     , INTERN );
COPY_SINGLE_VAR(  AlarmLogRegion4                             , AD     , INTERN );
COPY_SINGLE_VAR(  AlarmLogRegion5                             , AD     , INTERN );

CREATE_DLOG_MAP(AD, INTERN)
{
               /*---------------------------------------------------------------*/
               /*  Variable Name                                       , Prefix */
               /*---------------------------------------------------------------*/
  ADD_SINGLE_VAR(   AlarmLogRegion1                                    , AD     ),
  ADD_SINGLE_VAR(   AlarmLogRegion2                                    , AD     ),
  ADD_SINGLE_VAR(   AlarmLogRegion3                                    , AD     ),
  ADD_SINGLE_VAR(   AlarmLogRegion4                                    , AD     ),
  ADD_SINGLE_VAR(   AlarmLogRegion5                                    , AD     ),
};


/* *****************************************************************************
 *        EVENTS DATALOGGER INTERNAL DECLARATION
***************************************************************************** */
              /*----------------------------------------------------------------*/
              /*  Variable Name                               , Prefix , Local  */
              /*----------------------------------------------------------------*/
COPY_SINGLE_VAR(  EventType                                   , EVD     , INTERN );
COPY_SINGLE_VAR(  EventDate                                   , EVD     , INTERN );
COPY_SINGLE_VAR(  EventTime                                   , EVD     , INTERN );
COPY_SINGLE_VAR(  GenericArea1                                , EVD     , INTERN );
COPY_SINGLE_VAR(  GenericArea2                                , EVD     , INTERN );

CREATE_DLOG_MAP(EVD, INTERN)
{
               /*---------------------------------------------------------------*/
               /*  Variable Name                                       , Prefix */
               /*---------------------------------------------------------------*/
  ADD_SINGLE_VAR(   EventType                                          , EVD     ),
  ADD_SINGLE_VAR(   EventDate                                          , EVD     ),
  ADD_SINGLE_VAR(   EventTime                                          , EVD     ),
  ADD_SINGLE_VAR(   GenericArea1                                       , EVD     ),
  ADD_SINGLE_VAR(   GenericArea2                                       , EVD     ),
};


/* *****************************************************************************
 *        DATALOGGER MAP INFO TABLE
***************************************************************************** */
REFERENCE_MAP =
{
             /*--------,--------------------------,-----------------------------,---------,---------,-------------------------------*/
             /* Prefix , Application ID           , Memory ID                   , Records , Memory  , Type (Periodic / Event / VUR) */
             /*--------,--------------------------,-----------------------------,---------,---------,-------------------------------*/
  ADD_DLOG_REF( TD     , Transaction_Log          , AXIOM_01_TD_LOG_EEP_ID      , 100     , EXT_FLASH  , TYPE_EVENT(LogTriggerTD.Value)),
  ADD_DLOG_REF( BD     , Batch_Log                , AXIOM_01_BD_LOG_EEP_ID      , 100     , EXT_FLASH  , TYPE_EVENT(LogTriggerBD.Value)),
  ADD_DLOG_REF( SD     , Stream_Log               , AXIOM_01_SD_LOG_EEP_ID      , 100     , EXT_FLASH  , TYPE_EVENT(LogTriggerSD.Value)),
  ADD_DLOG_REF( AD     , Alarm_Log                , AXIOM_01_AD_LOG_EEP_ID      , 100     , EXT_FLASH  , TYPE_EVENT(LogTriggerAD.Value)),
  ADD_DLOG_REF( EVD    , Event_Log                , AXIOM_01_EVD_LOG_EEP_ID     , 100     , EXT_FLASH  , TYPE_EVENT(LogTriggerEVD.Value)),

};

const uint8 PTR_MAP_SIZE = (sizeof(DataloggerIDMaps)/sizeof(Dlog_Map_t));


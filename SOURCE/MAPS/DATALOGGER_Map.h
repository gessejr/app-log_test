/* *****************************************************************************
 FILE_NAME:     DATALOGGER_Map.h
 DESCRIPTION:   Datalogger map header
 DESIGNER:      Andre F. N. Dainese
 CREATION_DATE: 08/11/2017
 VERSION:       1.0
***************************************************************************** */

#ifndef SOURCES_DATALOG_MAP_H_
#define SOURCES_DATALOG_MAP_H_

#include "APP_INCLUDES.h"
/* *****************************************************************************
 *        TRANSACTION DATALOGGER EXTERNAL DECLARATION
***************************************************************************** */
              /*----------------------------------------------------------------*/
              /*  Variable Name                               , Prefix , Local  */
              /*----------------------------------------------------------------*/
COPY_SINGLE_VAR(  TransactionID                               , TD     , EXTERN );
COPY_SINGLE_VAR(  UserID                                      , TD     , EXTERN );
COPY_SINGLE_VAR(  TransStartDate                              , TD     , EXTERN );
COPY_SINGLE_VAR(  TransStartTime                              , TD     , EXTERN );
COPY_SINGLE_VAR(  TransStopDate                               , TD     , EXTERN );
COPY_SINGLE_VAR(  TransStopTime                               , TD     , EXTERN );
COPY_SINGLE_VAR(  WMIntended                                  , TD     , EXTERN );
COPY_SINGLE_VAR(  BaseTemperature                             , TD     , EXTERN );
COPY_SINGLE_VAR(  PresetType                                  , TD     , EXTERN );
COPY_SINGLE_VAR(  StartAddrBatch                              , TD     , EXTERN );
COPY_SINGLE_VAR(  EndAddrBatch                                , TD     , EXTERN );

CREATE_DLOG_MAP(TD, EXTERN);

/* *****************************************************************************
 *        BATCH DATALOGGER EXTERNAL DECLARATION
***************************************************************************** */
              /*----------------------------------------------------------------*/
              /*  Variable Name                               , Prefix , Local  */
              /*----------------------------------------------------------------*/
COPY_SINGLE_VAR(  BatchID                                     , BD     , EXTERN );
COPY_SINGLE_VAR(  ArmNumber                                   , BD     , EXTERN );
COPY_SINGLE_VAR(  ProductName                                 , BD     , EXTERN );
COPY_SINGLE_VAR(  BatchStartDate                              , BD     , EXTERN );
COPY_SINGLE_VAR(  BatchStartTime                              , BD     , EXTERN );
COPY_SINGLE_VAR(  BatchStopDate                               , BD     , EXTERN );
COPY_SINGLE_VAR(  BatchStopTime                               , BD     , EXTERN );
COPY_SINGLE_VAR(  WMCompilant                                 , BD     , EXTERN );
COPY_SINGLE_VAR(  PresetVolume                                , BD     , EXTERN );
COPY_SINGLE_VAR(  ReturnedQuantity                            , BD     , EXTERN );
COPY_SINGLE_VAR(  BatchGOV                                    , BD     , EXTERN );
COPY_SINGLE_VAR(  BatchGSV                                    , BD     , EXTERN );
COPY_SINGLE_VAR(  BatchMass                                   , BD     , EXTERN );
COPY_SINGLE_VAR(  BlendType                                   , BD     , EXTERN );
COPY_SINGLE_VAR(  StartAddrStream                             , BD     , EXTERN );
COPY_SINGLE_VAR(  EndAddrStream                               , BD     , EXTERN );


CREATE_DLOG_MAP(BD, EXTERN);

/* *****************************************************************************
 *        STREAM DATALOGGER EXTERNAL DECLARATION
***************************************************************************** */
              /*----------------------------------------------------------------*/
              /*  Variable Name                               , Prefix , Local  */
              /*----------------------------------------------------------------*/
COPY_SINGLE_VAR(  StreamID                                    , SD     , EXTERN );
COPY_SINGLE_VAR(  ProductName                                 , SD     , EXTERN );
COPY_SINGLE_VAR(  AccumStartGOV                               , SD     , EXTERN );
COPY_SINGLE_VAR(  AccumStopGOV                                , SD     , EXTERN );
COPY_SINGLE_VAR(  AccumStartGSV                               , SD     , EXTERN );
COPY_SINGLE_VAR(  AccumStopGSV                                , SD     , EXTERN );
COPY_SINGLE_VAR(  AccumStartMass                              , SD     , EXTERN );
COPY_SINGLE_VAR(  AccumStopMass                               , SD     , EXTERN );
COPY_SINGLE_VAR(  StreamGOV                                   , SD     , EXTERN );
COPY_SINGLE_VAR(  StreamGSV                                   , SD     , EXTERN );
COPY_SINGLE_VAR(  StreamMass                                  , SD     , EXTERN );
COPY_SINGLE_VAR(  AverageDensity                              , SD     , EXTERN );
COPY_SINGLE_VAR(  TempCompensation                            , SD     , EXTERN );


CREATE_DLOG_MAP(SD, EXTERN);

/* *****************************************************************************
 *        ALARM DATALOGGER EXTERNAL DECLARATION
***************************************************************************** */
              /*----------------------------------------------------------------*/
              /*  Variable Name                               , Prefix , Local  */
              /*----------------------------------------------------------------*/
COPY_SINGLE_VAR(  AlarmLogRegion1                             , AD     , EXTERN );
COPY_SINGLE_VAR(  AlarmLogRegion2                             , AD     , EXTERN );
COPY_SINGLE_VAR(  AlarmLogRegion3                             , AD     , EXTERN );
COPY_SINGLE_VAR(  AlarmLogRegion4                             , AD     , EXTERN );
COPY_SINGLE_VAR(  AlarmLogRegion5                             , AD     , EXTERN );


CREATE_DLOG_MAP(AD, EXTERN);

/* *****************************************************************************
 *        EVENTS DATALOGGER EXTERNAL DECLARATION
***************************************************************************** */
              /*----------------------------------------------------------------*/
              /*  Variable Name                               , Prefix , Local  */
              /*----------------------------------------------------------------*/
COPY_SINGLE_VAR(  EventType                                   , EVD    , EXTERN );
COPY_SINGLE_VAR(  EventDate                                   , EVD    , EXTERN );
COPY_SINGLE_VAR(  EventTime                                   , EVD    , EXTERN );
COPY_SINGLE_VAR(  GenericArea1                                , EVD    , EXTERN );
COPY_SINGLE_VAR(  GenericArea2                                , EVD    , EXTERN );


CREATE_DLOG_MAP(EVD, EXTERN)

#endif

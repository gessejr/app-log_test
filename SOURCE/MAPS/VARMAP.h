/* ************************************************************************************************************
 FILE_NAME:     VARMAP.h
 DESCRIPTION:   This file has a map of all variables available to the user
 DESIGNER:      Andre F. N. Dainese
 CREATION_DATE: dez/2015
 VERSION:       2.2
**************************************************************************************************************
Version 1.0:    10/sep/2015 - Andre F. N. Dainese
                - Initial implementation of the file
************************************************************************************************************ */
#ifndef VARMAP_H_INCLUDED
#define VARMAP_H_INCLUDED

#include "types.h"
#include "macros.h"


/* ************************************************************************************************************
 *
 *        DEFINES, ENUMS, STRUCTURES
 *
************************************************************************************************************ */

/* Definition of the structure used to  */
/* create the Config Table              */
typedef struct
  {
  void *        VarAddress;
  uint8         Length;
  VarType_t     Type;
  UserLevel_t   AccessLevel;
  Volatility_t  Volatility;
  LogChanges_t  LogChanges;
  uint16        ArrayLength;
  } ConfigTable_t;

/* Definition of the structure used to  */
/* create the Values Table              */
typedef struct
  {
  void *        VarAddress;
  uint8         Length;
  VarType_t     Type;
  UserLevel_t   AccessLevel;
  Volatility_t  Volatility;
  } OutputTable_t;

/* Macros used to add new item in ConfigTable */
#define ADD_CONFIG_SNG_ITEM( VAR, TYPE, LEVEL, VOLAT, LOG )     { &VAR.Current,     sizeof(VAR.Current),    TYPE, LEVEL, VOLAT, LOG,  1                                           }
#define ADD_CONFIG_MAT_ITEM( VAR, TYPE, LEVEL, VOLAT, LOG, I)   { &VAR.Current[I],  sizeof(VAR.Current[I]), TYPE, LEVEL, VOLAT, LOG,  sizeof(VAR.Current)/sizeof(VAR.Current[I])  }

/* Macros used to add new item in OutputTable */
#define ADD_OUTPUT_SNG_ITEM( VAR, TYPE, LEVEL, VOLAT)           { &VAR.Value,       sizeof(VAR.Value),      TYPE, LEVEL, VOLAT  }
#define ADD_OUTPUT_MAT_ITEM( VAR, TYPE, LEVEL, VOLAT, I)        { &VAR.Value[I],    sizeof(VAR.Value[I]),   TYPE, LEVEL, VOLAT  }



/* ************************************************************************************************************
 *
 *        APPLICATION GLOBAL VARIABLES
 *
************************************************************************************************************ */
extern const ConfigTable_t  VarMapConfigTable[];
extern const OutputTable_t  VarMapOutputTable[];
extern const uint16 VarMapConfigLen;
extern const uint16 VarMapOutputLen;


/* ************************************************************************************************************
 *
 *        LIBRARIES EXTERN VARIABLES - TO BE DEPRECATED!
 *
************************************************************************************************************ */
#define ADD_OUTPUT_MDB_ITEM( VAR, TYPE, LEVEL, VOLAT)           { &VAR.Current,       sizeof(VAR.Current),      TYPE, LEVEL, VOLAT}

#endif

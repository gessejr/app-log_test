/*
 * STORAGE_MAP.h
 *
 *  Created on: 14/06/2016
 *      Author: DENISBERALDO
 */

#ifndef SOURCES_MAP_STORAGE_MAP_H_
#define SOURCES_MAP_STORAGE_MAP_H_

#include "types.h"
#include "STORAGE_Link.h"

//#define ENABLE_MEM_RAM_FUNCTIONS
//#include "MEM_RAM.h"

//#define ENABLE_MEM_RAM_FUNCTIONS
//#include "MEM_RAM.h"

#define ENABLE_FLASH_FUNCTIONS
#define ENABLE_FLASH_LOWER_OP_FUNCTIONS
#define ENABLE_FLASH_LOWER_MNG_FUNCTIONS
#include "FLASH.H"

extern const Str_Config_Map_t STORAGE_MAP[];

#endif /* SOURCES_MAP_STORAGE_MAP_H_ */
